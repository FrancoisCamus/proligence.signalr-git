using Orchard.UI.Resources;

namespace Proligence.SignalR {
    public class ResourceManifest : IResourceManifestProvider {
        public void BuildManifests(ResourceManifestBuilder builder) {
            var manifest = builder.Add();
            manifest.DefineScript("jQuery_SignalR").SetUrl("jquery.signalR.min.js", "jquery.signalR.js").SetVersion("2.2.0").SetDependencies("jQuery");
            manifest.DefineScript("jQuery_SignalR_Hubs").SetUrl("~/signalr/hubs").SetVersion("2.2.0").SetDependencies("jQuery_SignalR");
        }
    }
}
